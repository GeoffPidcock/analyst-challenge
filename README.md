# analyst-challenge

This repo contains a jupyter notebook discussing both SQL and python code to address tasks 1 and 2 of the Canva data analyst challenge.
The SQL code is written to be PostGres 9.6+ compliant. 
The Python code uses a python 3.7 anaconda distribution, with the wonderful [pandas profiling](https://github.com/pandas-profiling/pandas-profiling) package installed. See requirements.txt for further details. 

## Important Note:
Task 2 is accompanied by a [summary slide deck, which can be viewed here](https://www.canva.com/design/DADYxT74cIs/28SYI11hjTEozF-bSJ2Bxg/view?utm_content=DADYxT74cIs&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton).
Please reach out to Canva for details regarding the data or the challenge. 

## Acknowledgements
- Jos Polfliet for building the wonderful pandas profiling package - [link](https://github.com/pandas-profiling/pandas-profiling)